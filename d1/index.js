//Notes:
//the document refers to the whole webpage
//to access specific object models from document we can use:
//document.querySelector('#txt-first-name')

//document.getElementById('txt-first-name')
//document.getElementsByClassName('txt-inputs')
//document.getElementsByTagName('input')

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');

const spanFullName = document.querySelector('#span-full-name');
const spanFullName2 = document.querySelector('#span-full-name2');

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
	;
	
//innerHTML parang console.log
});
//Multiple listener can also be assigned to the same event 
txtLastName.addEventListener('keyup', (event) =>{
	// console.log(event.target);
	// console.log(event.target.value); //similaar to the txtFirstName.value
	spanFullName2.innerHTML = txtLastName.value;

});